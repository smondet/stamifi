Stamifi: Standard Midi Files in OCaml
=====================================

This library is extracted from the venerable
[Locoseq](https://github.com/smondet/locoseq).


Build/Install
-------------

See `stamifi.opam`.

Tests
-----

For now the test just prints out any input midi-file:

    _build/default/src/test/test_rw.exe src/test/data/just-drums.mid
    

