(******************************************************************************)
(*      Copyright (c) 2007,2017 Sebastien MONDET                                  *)
(*                                                                            *)
(*      Permission is hereby granted, free of charge, to any person           *)
(*      obtaining a copy of this software and associated documentation        *)
(*      files (the "Software"), to deal in the Software without               *)
(*      restriction, including without limitation the rights to use,          *)
(*      copy, modify, merge, publish, distribute, sublicense, and/or sell     *)
(*      copies of the Software, and to permit persons to whom the             *)
(*      Software is furnished to do so, subject to the following              *)
(*      conditions:                                                           *)
(*                                                                            *)
(*      The above copyright notice and this permission notice shall be        *)
(*      included in all copies or substantial portions of the Software.       *)
(*                                                                            *)
(*      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       *)
(*      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES       *)
(*      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND              *)
(*      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT           *)
(*      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,          *)
(*      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING          *)
(*      FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR         *)
(*      OTHER DEALINGS IN THE SOFTWARE.                                       *)
(******************************************************************************)

(** Values allowing to implement the MIDI standard. *)

module Midi_event = struct
  type t = {
    ticks : int;
    status : int;
    channel : int;
    data_1 : int;
    data_2 : int; (* mutable cmd     : midi_cmd ; *)
  }

  let empty = { ticks = 0; status = 0; channel = 0; data_1 = 0; data_2 = 0 }

  let make ~ticks ~status ~channel ~data_1 ~data_2 =
    { ticks; status; channel; data_1; data_2 }

  let pp fmt { ticks; status; channel; data_1; data_2 } =
    let open Format in
    fprintf fmt "{T: %d;@ S: %x;@ C: %d;@ D1: %d;@ D2: %d}" ticks status channel
      data_1 data_2
end

module Meta_event = struct
  type t = { ticks : int; service_id : int; service_data : int array }

  let make ~ticks ~service_id ~service_data =
    { ticks; service_id; service_data }

  let pp fmt { ticks; service_id; service_data } =
    let open Format in
    fprintf fmt "{T: %d;@ Id: %x;@ Data: " ticks service_id;
    ArrayLabels.iter service_data ~f:(fun d -> fprintf fmt "%x;" d);
    fprintf fmt "}"
end

module Sys_event = struct
  (* Not (yet) supported: *)
  type t = unit
end

module Event = struct
  type t =
    | Empty
    | Midi of Midi_event.t
    | Meta of Meta_event.t
    | Sys of Sys_event.t

  let empty = Empty
  (* let max_midi_val = ref 127 *)

  let pp fmt =
    let open Format in
    function
    | Empty -> fprintf fmt "(Empty)"
    | Midi e -> fprintf fmt "@[(Midi@ %a)@]" Midi_event.pp e
    | Meta e -> fprintf fmt "@[(Meta@ %a)@]" Meta_event.pp e
    | Sys _ -> fprintf fmt "(System NOT-IMPLEMENTED)"
end

module Track = struct
  type t = {
    mutable id : int32;
    mutable length : int32;  (** Length in bytes *)
    mutable events : Event.t array;
  }

  let empty () = { id = 0l; length = 0l; events = [||] }
  let make_events nb = Array.init nb (fun _x -> Event.empty)
end

module File = struct
  type t = {
    mutable id : int32;
    mutable header_length : int32;
    mutable midi_format : int;
    (* "short" values (16 bits) *)
    mutable track_number : int;
    (* "short" values (16 bits) *)
    mutable per_quarter_note : int;
    (* "short" values (16 bits) *)
    mutable tracks : Track.t array;
  }

  let make_tracks nb = Array.init nb (fun _x -> Track.empty ())

  let empty () =
    {
      id = 0l;
      header_length = 0l;
      midi_format = 0;
      track_number = 0;
      per_quarter_note = 0;
      tracks = [||];
    }
end

(******************************************************************************)
(*
 * Some Help about meta-event services:
 *
 * *)
type known_meta_service_util = {
  s_id : int;
  s_name : string;
  s_data_to_str : (int array -> string) option;
}

let basic_data_to_string data =
  (* int array -> string *)
  let lgth = Array.length data in
  let str = Bytes.make lgth '\000' in
  for i = 0 to lgth - 1 do
    Bytes.set str i (char_of_int data.(i))
  done;
  Bytes.to_string str

let known_meta_services =
  let d2s = Some basic_data_to_string in
  let short =
    Some
      (fun data ->
        if Array.length data = 2 then string_of_int ((data.(0) * 256) + data.(1))
        else "Err")
  in
  let no = None in
  [
    { s_id = 0x00; s_name = "SequenceNumber         "; s_data_to_str = short };
    { s_id = 0x01; s_name = "TextEvent              "; s_data_to_str = d2s };
    { s_id = 0x02; s_name = "CopyrightNotice        "; s_data_to_str = d2s };
    { s_id = 0x03; s_name = "SequenceOrTrackName    "; s_data_to_str = d2s };
    { s_id = 0x04; s_name = "InstrumentName         "; s_data_to_str = d2s };
    { s_id = 0x05; s_name = "LyricText              "; s_data_to_str = d2s };
    { s_id = 0x06; s_name = "MarkerText             "; s_data_to_str = d2s };
    { s_id = 0x07; s_name = "CuePoint               "; s_data_to_str = no };
    { s_id = 0x20; s_name = "MIDIChannelPrefixAssign"; s_data_to_str = no };
    { s_id = 0x2F; s_name = "EndOfTrack             "; s_data_to_str = no };
    { s_id = 0x51; s_name = "TempoSetting           "; s_data_to_str = no };
    { s_id = 0x54; s_name = "SMPTEOffset            "; s_data_to_str = no };
    { s_id = 0x58; s_name = "TimeSignature          "; s_data_to_str = no };
    { s_id = 0x59; s_name = "KeySignature           "; s_data_to_str = no };
    { s_id = 0x7F; s_name = "SequencerSpecificEvent "; s_data_to_str = no };
  ]

(******************************************************************************)

(** Higher-level midi events. *)
module Midi_command = struct
  type t =
    | Empty
    | Unkown of Midi_event.t
    | Note_off of (int * int) (* note nb , velocity *)
    | Note_on of (int * int) (* note nb , velocity *)
    | After_touch of (int * int)
    | Control_change of (int * int)
    | Program_change of int
    | Channel_after_touch of int
    | Pitch_range of (int * int)
  [@@deriving show]

  let make_2B_cmd run_stat note velo =
    let ret =
      match run_stat with
      | rs when 0x80 <= rs && rs <= 0x8F -> Note_off (note, velo)
      | rs when 0x90 <= rs && rs <= 0x9F ->
          if velo = 0 (* If velocity = 0 -> note off ! *) then
            Note_off (note, velo)
          else Note_on (note, velo)
      | rs when 0xA0 <= rs && rs <= 0xAF -> After_touch (note, velo)
      | rs when 0xB0 <= rs && rs <= 0xBF ->
          (* if ( 0x0 < note && note < 0x7F) *)
          (* then *)
          Control_change (note, velo)
          (* else  *)
          (* (failwith "Error: Bad ControlChanged event"); *)
      | rs when 0xE0 <= rs && rs <= 0xEF -> Pitch_range (note, velo)
      | _rs -> failwith "Error: unknown 2 bytes command!!"
    in
    ret

  let make_1B_cmd run_stat note =
    match run_stat with
    | rs when 0xC0 <= rs && rs <= 0xCF -> Program_change note
    | rs when 0xD0 <= rs && rs <= 0xDF -> Channel_after_touch note
    | _ -> failwith "Error: unknown 1 byte command !!"

  let of_midi_event ev =
    let open Midi_event in
    match ev.status with
    (* It is a 2 bytes MIDI EVENT: *)
    | rs when (0x80 <= rs && rs <= 0xBF) || (0xE0 <= rs && rs <= 0xEF) ->
        make_2B_cmd rs ev.data_1 ev.data_2
    (* It is a 1 byte MIDI EVENT: *)
    | rs when 0xC0 <= rs && rs <= 0xDF -> make_1B_cmd rs ev.data_1
    | _ -> Unkown ev
end
