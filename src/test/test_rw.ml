open Printf

let prf fmt = ksprintf (printf "%s%!") fmt
let line fmt = ksprintf (prf "%s\n%!") fmt

let () =
  Printf.printf "Stamifi: %s\n%!" Stamifi.Meta.version;
  match Sys.argv.(1) with
  | path -> ignore (Stamifi.Midi_file.parse_smf path)
  | exception _ ->
      line "usage: %s <path>" Sys.argv.(0);
      exit 1
